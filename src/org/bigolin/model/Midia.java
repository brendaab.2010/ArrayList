
package org.bigolin.model;

/**
 *
 * @author Brenda Barbosa
 */
public class Midia {
     private String nome;

    private int duracao;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }


    public int getDuracao() {
        return duracao;
    }

    public void setDuracao(int duracao) {
        this.duracao = duracao;
    }

    @Override
    public String toString() {
        return "Midia{" + "nome=" + nome +  ", duracao=" + duracao + '}';
    }
}
