package org.bigolin.bigoflix;

/**
 * @author Brenda Barbosa
 */

import java.util.Scanner;
import org.bigolin.model.Filme;
import org.bigolin.model.Lista;

public class BigoflixTeste {

    public static void main(String[] args) {
        Lista preferidos = new Lista();
        
        Filme f1 = new Filme();
        f1.setNome("O Senhor dos Anéis: O Retorno do Rei");
        f1.setDiretor("Peter Jackson"); 
        f1.setDuracao(38000);
        Filme f2 = new Filme();
        f2.setNome("Titanic");
        f2.setDiretor("James Cameron");
        f2.setDuracao(50000);
        Filme f3 = new Filme();
        f3.setNome("E.T. – O Extraterrestre");
        f3.setDiretor("Steven Spielberg");
        f3.setDuracao(28000);
        
        preferidos.add(f1);     
        preferidos.add(f2);
        preferidos.mostra();
        preferidos.remove(f1);
        preferidos.add(f3);
        preferidos.mostra();
    }    
}
